// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterAS_Component.h"

void UCharacterAS_Component::OnAttributeUpdated(const FOnAttributeChangeData& Data)
{
	OnAttributeChange.Broadcast(Data.Attribute,Data.NewValue);
}

void UCharacterAS_Component::OnTagUpdated(const FGameplayTag& Tag, bool TagExists){
	OnTagChange.Broadcast(Tag,TagExists);
}

void UCharacterAS_Component::RegisterAttributeToDelegate(FGameplayAttribute Attribute)
{
	GetGameplayAttributeValueChangeDelegate(Attribute).AddUObject(this,&UCharacterAS_Component::OnAttributeUpdated);
}
