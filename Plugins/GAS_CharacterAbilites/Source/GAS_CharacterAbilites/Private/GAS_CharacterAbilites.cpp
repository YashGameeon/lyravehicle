// Copyright Epic Games, Inc. All Rights Reserved.

#include "GAS_CharacterAbilites.h"

#define LOCTEXT_NAMESPACE "FGAS_CharacterAbilitesModule"

void FGAS_CharacterAbilitesModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
}

void FGAS_CharacterAbilitesModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FGAS_CharacterAbilitesModule, GAS_CharacterAbilites)