// Fill out your copyright notice in the Description page of Project Settings.


#include "GAS_PlayerGameplayAbility.h"

void UGAS_PlayerGameplayAbility::InputReleased(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo)
{
	if (ActorInfo != NULL && ActorInfo->AvatarActor != NULL)
	{
		OnInputReleased(); //Replaced with a delegate to have flexibility
	}
}

void UGAS_PlayerGameplayAbility::OnInputReleased_Implementation()
{
	OnInputReleasedDelegate.Broadcast();
}