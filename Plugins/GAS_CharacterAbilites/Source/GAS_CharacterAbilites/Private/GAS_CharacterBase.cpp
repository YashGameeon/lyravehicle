// Fill out your copyright notice in the Description page of Project Settings.


#include "GAS_CharacterBase.h"
#include "CharacterAttributeSet.h"
#include "AbilitySystemComponent.h"
#include "CharacterGameplayEffect.h"
#include "Net/UnrealNetwork.h"

// Sets default values
AGAS_CharacterBase::AGAS_CharacterBase()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	CharacterAttributes = CreateDefaultSubobject<UCharacterAttributeSet>(TEXT("Character Attributes"));
	AbilitySystemComponent = CreateDefaultSubobject<UCharacterAS_Component>(TEXT("AbilitySystemComponent"));
	AbilitySystemComponent->SetIsReplicated(true);
	AbilitySystemComponent->SetReplicationMode(EGameplayEffectReplicationMode::Full);
}

// Called when the game starts or when spawned
void AGAS_CharacterBase::BeginPlay()
{
	Super::BeginPlay();
	check(AbilitySystemComponent);
}

// Called every frame
void AGAS_CharacterBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AGAS_CharacterBase::PossessedBy(AController* NewController )
{
	Super::PossessedBy(NewController);
	AbilitySystemComponent->InitAbilityActorInfo(this,this);
	InitializeEffects();
	GiveAbilities();
}

// Called to bind functionality to input
void AGAS_CharacterBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	
	if(AbilitySystemComponent && PlayerInputComponent)
	{
		//const FGameplayAbilityInputBinds Binds("Confirm" , "Cancel" , "EInputEnum" , static_cast<int32>(EInputEnum::Confirm), static_cast<int32>(EInputEnum::Cancel));
		//AbilitySystemComponent->BindAbilityActivationToInputComponent(InputComponent,Binds);
	}
}

void AGAS_CharacterBase::InitializeEffects()
{
	for (TSubclassOf<UCharacterGameplayEffect> Effect : DefaultEffects)
	{
		AddEffect(Effect);
	}
}

void AGAS_CharacterBase::AddEffect(TSubclassOf<UCharacterGameplayEffect> Effect)
{
	if(!HasAuthority())
	{
		ServerAddEffect(Effect);
	}
	if(AbilitySystemComponent)
	{
		TSubclassOf<UCharacterGameplayEffect>& EffectRef = Effect;

		FGameplayEffectContextHandle EffectContext = AbilitySystemComponent->MakeEffectContext();
		EffectContext.AddSourceObject(this);

		FGameplayEffectSpecHandle EffectSpecHandle = AbilitySystemComponent->MakeOutgoingSpec(Effect,1,EffectContext);

		if(EffectSpecHandle.IsValid())
		{
			FName Id = EffectRef.GetDefaultObject()->EffectId;
			FActiveGameplayEffectHandle EffectHandle = AbilitySystemComponent->ApplyGameplayEffectSpecToSelf(*EffectSpecHandle.Data);
			Effects.Add(FEffectKeyPair(Id,EffectHandle));
		}
	}
}
bool AGAS_CharacterBase::ServerAddEffect_Validate(TSubclassOf<UCharacterGameplayEffect> Effect)
{
	return true;
}
void AGAS_CharacterBase::ServerAddEffect_Implementation(TSubclassOf<UCharacterGameplayEffect> Effect)
{
	
}

void AGAS_CharacterBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AGAS_CharacterBase,Effects);
}

class UAbilitySystemComponent* AGAS_CharacterBase::GetAbilitySystemComponent()const
{
	return AbilitySystemComponent;
}

void AGAS_CharacterBase::GiveAbilities()
{
	if(HasAuthority()&&AbilitySystemComponent)
	{
		for(TSubclassOf<UGAS_PlayerGameplayAbility>&Ability: DefaultAbilities)
		{
			AbilitySystemComponent->GiveAbility(FGameplayAbilitySpec(Ability,1,Ability.GetDefaultObject()->InputID,this));
		}
	}
}

