// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gas_InputEnum.h"
#include "Abilities/GameplayAbility.h"
#include "GAS_PlayerGameplayAbility.generated.h"

/**
 * 
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FInputReleased);

UCLASS()
class GAS_CHARACTERABILITES_API UGAS_PlayerGameplayAbility : public UGameplayAbility
{
	GENERATED_BODY()

	
	

public:
	UPROPERTY(BlueprintAssignable)
	FInputReleased OnInputReleasedDelegate;

	//What Button input should this ability fire on? Call the respective int from the ability system component to fire the ability
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Ability")
	int32 InputID;

	//Should the ability be cancelled if the input is released
	//UPROPERTY(BlueprintReadWrite,EditAnywhere,Category = "Ability")
	//bool bCancelOnInputRelease;

	
	//Override for release input function
	virtual void InputReleased(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo) override;

	UFUNCTION(BlueprintNativeEvent, Category = "Calculation")
	void OnInputReleased();

};
