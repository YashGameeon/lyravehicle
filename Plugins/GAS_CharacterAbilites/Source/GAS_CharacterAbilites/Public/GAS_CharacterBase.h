// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "CharacterAttributeSet.h"
#include "CharacterGameplayEffect.h"
#include "AbilitySystemComponent.h"
#include "AbilitySystemInterface.h"
#include "CharacterAS_Component.h"
#include "Gas_PlayerGameplayAbility.h"
#include "GAS_CharacterBase.generated.h"

UCLASS()
class GAS_CHARACTERABILITES_API AGAS_CharacterBase : public ACharacter,public IAbilitySystemInterface
{
	GENERATED_BODY()
	UCharacterAttributeSet* CharacterAttributes;
public:
	//UPROPERTY(BlueprintReadWrite,EditAnywhere, Category = "Attributes")
	//TSubclassOf<UCharacterAttributeSet> CharacterAttributeSet;

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly, Category = "Abilities")
	UCharacterAS_Component* AbilitySystemComponent;

	
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly, Category = "Abilities")
	TArray<TSubclassOf<UGAS_PlayerGameplayAbility>> DefaultAbilities;

	UPROPERTY(BlueprintReadOnly,EditAnywhere, Category = "Effects")
	TArray<TSubclassOf<UCharacterGameplayEffect>> DefaultEffects;
	// Sets default values for this character's properties

	UPROPERTY(Replicated,BlueprintReadWrite, Category="Effects")
	TArray<FEffectKeyPair> Effects;

	AGAS_CharacterBase();

	virtual void PossessedBy(AController* NewController) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	

	UFUNCTION(BlueprintCallable,Category="Effects")
	void InitializeEffects();

	UFUNCTION(BlueprintCallable,Category="Effects")
	void AddEffect(TSubclassOf<UCharacterGameplayEffect> Effect);

	UFUNCTION(RELIABLE,Server,WithValidation)
	void ServerAddEffect(TSubclassOf<UCharacterGameplayEffect> Effect);
	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual class UAbilitySystemComponent* GetAbilitySystemComponent() const override;

	virtual void GiveAbilities();
};
