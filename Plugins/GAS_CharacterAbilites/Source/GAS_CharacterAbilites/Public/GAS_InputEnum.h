// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */

UENUM(BlueprintType)
enum class EInputEnum:uint8
{
	None UMETA(DisplayName="None"),
	Fire UMETA(DisplayName="Fire"),
	Crouch UMETA(DisplayName="Crouch"),
	Jump UMETA(DisplayName="Jump"),
	Confirm,
	Cancel
};