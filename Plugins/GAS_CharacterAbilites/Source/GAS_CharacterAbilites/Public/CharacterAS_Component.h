// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemComponent.h"
#include "CharacterAS_Component.generated.h"

/**
 * 
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FAttributeUpdated,const FGameplayAttribute&,AttributeChanged,float,NewAttributeValue);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FTagUpdated,const FGameplayTag&,Tag,bool,TagExists);

UCLASS()
class GAS_CHARACTERABILITES_API UCharacterAS_Component : public UAbilitySystemComponent
{
	GENERATED_BODY()

private:
	virtual void OnAttributeUpdated(const FOnAttributeChangeData & Data);
	virtual void OnTagUpdated(const FGameplayTag& Tag, bool TagExists) override;
public:
	//Called when an attribute is updated. The attribute in concern must be regestered to the Delegate RegisterAttributeToDelegate.
	UPROPERTY(BlueprintAssignable)
	FAttributeUpdated OnAttributeChange;

	//Called when a tag is updated.
	UPROPERTY(BlueprintAssignable)
	FTagUpdated OnTagChange;

	//Refisters the attribute to the OnAttributeChange Delegate
	UFUNCTION(BlueprintCallable)
	void RegisterAttributeToDelegate(FGameplayAttribute Attribute);
};
