// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayEffect.h"
#include "CharacterGameplayEffect.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct FEffectKeyPair
{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effects")
	FName Id;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effects")
	FActiveGameplayEffectHandle EffectHandle;

public:
	FEffectKeyPair();

	FEffectKeyPair(FName id,FActiveGameplayEffectHandle handle);
};



UCLASS()
class GAS_CHARACTERABILITES_API UCharacterGameplayEffect : public UGameplayEffect
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadOnly,EditAnywhere,Category = "Effects")
	FName EffectId;
};
